eu_vat = Spree::Zone.find_by_name!("EU_VAT")
eu_vat.default_tax = true
eu_vat.save!
beverage = Spree::TaxCategory.find_by_name!("Beverage")
tax_rate = Spree::TaxRate.create(
  :name => "Denmark",
  :zone => eu_vat,
  :amount => 0.25,
  :included_in_price => true,
  :tax_category => beverage)
tax_rate.calculator = Spree::Calculator::DefaultTax.create!
tax_rate.save!
