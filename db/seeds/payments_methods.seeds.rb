if ENV['RAILS_ENV'] == 'production'
  stripe = Spree::Gateway::StripeGateway.create!(
    {
      :name => "Credit Card",
      :description     => "Live Stripe gateway for production.",
      :environment     => "production",
      :active          => true
    }
  )
elsif ENV['RAILS_ENV'] == 'staging'
  stripe = Spree::Gateway::StripeGateway.create!(
    {
      :name => "Credit Card",
      :description     => "Test Stripe gateway for staging.",
      :environment     => "staging",
      :active          => true
    }
  )
end
unless stripe.nil?
  stripe.set_preference('secret_key', ENV['STRIPE_SEC'])
  stripe.set_preference('publishable_key', ENV['PUBLISHABLE_KEY'])
end


Spree::Gateway::Bogus.create!(
  {
    :name => "Credit Card",
    :description => "Bogus payment gateway for development.",
    :environment => "development",
    :active => true
  }
)

Spree::Gateway::Bogus.create!(
  {
    :name => "Credit Card",
    :description => "Bogus payment gateway for production.",
    :environment => "production",
    :active => false
  }
)

Spree::Gateway::Bogus.create!(
  {
    :name => "Credit Card",
    :description => "Bogus payment gateway for staging.",
    :environment => "staging",
    :active => false
  }
)

Spree::Gateway::Bogus.create!(
  {
    :name => "Credit Card",
    :description => "Bogus payment gateway for test.",
    :environment => "test",
    :active => true
  }
)

Spree::PaymentMethod::Check.create!(
  {
    :name => "Check",
    :description => "Pay by check.",
    :active => true
  }
)

