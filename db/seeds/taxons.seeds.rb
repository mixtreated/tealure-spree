after :taxonomies, :products do
  family = Spree::Taxonomy.find_by_name!("Tea family")
  products = {}

  [
    "Spring White",
    "Black Ruby",
    "Himalayan Gold",
    "Himalayan Emerald",
    "Himalayan Bouquet",
    "Silver Tips"
  ].each do |name|
    products[name.parameterize.underscore.to_sym] = Spree::Product.find_by_name!(name)
  end

  taxons = [
    {
      :name => "Teas",
      :taxonomy => family,
      :position => 0
    },
    {
      :name => "Green",
      :taxonomy => family,
      :parent => "Tea family",
      :position => 1,
      :products => [
        products[:himalayan_emerald]
      ]
    },
    {
      :name => "Black",
      :taxonomy => family,
      :parent => "Tea family",
      :position => 2,
      :products => [
        products[:black_ruby],
        products[:himalayan_gold]
      ]
    },
    {
      :name => "White",
      :taxonomy => family,
      :parent => "Tea family",
      :position => 3,
      :products => [
        products[:spring_white],
        products[:silver_tips]
      ]
    },
    {
      :name => "Oolong",
      :taxonomy => family,
      :parent => "Tea family",
      :position => 0,
     :products => [
        products[:himalayan_bouquet]
      ]
    }
  ]

  taxons.each do |taxon_attrs|
    if taxon_attrs[:parent]
      taxon_attrs[:parent] = Spree::Taxon.find_by_name!(taxon_attrs[:parent])
      Spree::Taxon.create!(taxon_attrs)
    end
  end
end
