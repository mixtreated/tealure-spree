products =
  {
    "Spring White" =>
    {
      "Type" => "White tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "1 heaping tsp.",
      "Re-steep" => "2 re-steeps",
      "Steeping" => "3 min. at 70 to 80°C",
    },
    "Black Ruby" =>
    {
      "Type" => "Black tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "1 heaping tsp.",
      "Re-steep" => "2 re-steeps",
      "Steeping" => "3 to 4 min. at 95°C",
    },
    "Himalayan Gold" =>
    {
      "Type" => "Black tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "1 heaping tsp.",
      "Re-steep" => "2 re-steeps",
      "Steeping" => "4 to 5 min. at 95°C",
    },
    "Himalayan Emerald" =>
    {
      "Type" => "Green tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "2 tsp.",
      "Re-steep" => "2 re-steeps",
      "Steeping" => "3 min. at 80°C",
    },
    "Himalayan Bouquet" =>
    {
      "Type" => "Oolong tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "1 heaping tsp.",
      "Re-steep" => "2 re-steeps",
      "Steeping" => "3 to 4 min. at 90°C",
    },
    "Silver Tips" =>
    {
      "Type" => "White tea",
      "Origin" => "Ilam, Nepal",
      "Serving quantity" => "2 tsp.",
      "Re-steep" => "3 re-steeps",
      "Steeping" => "4 to 5 min. at 80°C",
    }
  }

products.each do |name, properties|
  product = Spree::Product.find_by_name(name)
  properties.each do |prop_name, prop_value|
    product.set_property(prop_name, prop_value)
  end
end
