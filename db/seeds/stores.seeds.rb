unless Spree::Store.where(code: 'tealure').exists?
  Spree::Store.new do |s|
    s.code              = 'tealure'
    s.name              = 'Tealure store'
    s.url               = 'tealure.com'
    s.mail_from_address = 'info@tealure.com'

    s.default           = 'true'
  end.save!
end
