after :products, :variants do

  products = {}

  def self.image(name, type="jpg")
    images_path = Pathname.new(File.dirname(__FILE__)) + "images"
    path = images_path + "#{name}.#{type}"
    return false if !File.exist?(path)
    File.open(path)
  end

  [
    "Spring White",
    "Black Ruby",
    "Himalayan Gold",
    "Himalayan Emerald",
    "Himalayan Bouquet",
    "Silver Tips"
  ].each do |product_name|
    products[product_name.parameterize.underscore.to_sym] = Spree::Product.find_by_name!(product_name)

    # - Master variant images
    attachments = [
      {
        :attachment => image(product_name.parameterize.underscore, "png")
      },
      {
        :attachment => image(product_name.parameterize.underscore + "_cup", "jpg")
      }
    ]

    master = products[product_name.parameterize.underscore.to_sym].master
    puts "Loading images for #{master.product.name}"
    attachments.each do |attachment|
      master.images.create!(attachment)
    end

    # - Variants images
    products[product_name.parameterize.underscore.to_sym].variants.each do |variant|
      packaging = variant.option_values.detect { |o| o.option_type.name == "packaging" }.try(:name).parameterize.underscore
      main_image = image(product_name.parameterize.underscore + "_" + packaging)
      variant.images.create!(:attachment => main_image)
    end
  end
end
