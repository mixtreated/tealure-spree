after :option_values, :products do
  packaging = Spree::OptionType.find_by_name!("packaging")

  [
    "Spring White",
    "Black Ruby",
    "Himalayan Gold",
    "Himalayan Emerald",
    "Himalayan Bouquet",
    "Silver Tips",
  ].each do |product_name|
    instance_variable_set("@#{product_name.parameterize.underscore}", Spree::Product.find_by_name!(product_name))
    instance_variable_get("@#{product_name.parameterize.underscore}").option_types = [packaging]
    instance_variable_get("@#{product_name.parameterize.underscore}").save!
  end

  [
    "Pouch",
    "Lokta",
    "Bamboo",
  ].each do |option|
    instance_variable_set("@#{option.parameterize.underscore}", Spree::OptionValue.find_by_name!(option))
  end

  variants = [
    {
      :product => @spring_white,
      :option_values => [@pouch],
      :sku => "TEA00010",
      :price => "104.00",
      :weight => 50,
    },
    {
      :product => @spring_white,
      :option_values => [@lokta],
      :sku => "TEA00020",
      :price => "119.00",
      :weight => 50,
    },
    {
      :product => @spring_white,
      :option_values => [@bamboo],
      :sku => "TEA00030",
      :price => "109.00",
      :weight => 40,
    },
    {
      :product => @black_ruby,
      :option_values => [@pouch],
      :sku => "TEA00050",
      :price => "94.00",
      :weight => 50,
    },
    {
      :product => @black_ruby,
      :option_values => [@lokta],
      :sku => "TEA00060",
      :price => "109.00",
      :weight => 50,
    },
    {
      :product => @himalayan_gold,
      :option_values => [@pouch],
      :sku => "TEA00090",
      :price => "94.00",
      :weight => 50,
    },
    {
      :product => @himalayan_gold,
      :option_values => [@lokta],
      :sku => "TEA00100",
      :price => "109.00",
      :weight => 50,
    },
    {
      :product => @himalayan_gold,
      :option_values => [@bamboo],
      :sku => "TEA00110",
      :price => "99.00",
      :weight => 40,
    },
    {
      :product => @himalayan_emerald,
      :option_values => [@pouch],
      :sku => "TEA00130",
      :price => "49.00",
      :weight => 50,
    },
    {
      :product => @himalayan_emerald,
      :option_values => [@lokta],
      :sku => "TEA00140",
      :price => "64",
      :weight => 50,
    },
    {
      :product => @himalayan_bouquet,
      :option_values => [@pouch],
      :sku => "TEA00170",
      :price => "64.00",
      :weight => 50,
    },
    {
      :product => @himalayan_bouquet,
      :option_values => [@lokta],
      :sku => "TEA00180",
      :price => "79.00",
      :weight => 50,
    },
    {
      :product => @silver_tips,
      :option_values => [@lokta],
      :sku => "TEA00220",
      :price => "129.00",
      :weight => 50,
    },
    {
      :product => @silver_tips,
      :option_values => [@bamboo],
      :sku => "TEA00230",
      :price => "119.00",
      :weight => 40,
    },
  ]

  masters = {
    @spring_white => {
      :sku => "TEA10010",
      :price => "104.00",
    },
    @black_ruby => {
      :sku => "TEA10050",
      :price => "94.00",
    },
    @himalayan_gold => {
      :sku => "TEA10090",
      :price => "94.00",
    },
    @himalayan_emerald => {
      :sku => "TEA10130",
      :price => "49.00",
    },
    @himalayan_bouquet => {
      :sku => "TEA10170",
      :price => "64.00",
    },
    @silver_tips => {
      :sku => "TEA10210",
      :price => "119.00",
    }
  }

  Spree::Variant.create!(variants)

  masters.each do |product, variant_attrs|
    product.master.update_attributes!(variant_attrs)
  end
end
