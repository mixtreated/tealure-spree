after :products do
  packaging = Spree::OptionType.find_by_presentation!("Packaging")

  [
    "Spring White",
    "Black Ruby",
    "Himalayan Gold",
    "Himalayan Emerald",
    "Himalayan Bouquet",
    "Silver Tips"
  ].each do |product_name|
    instance_variable_set("@#{product_name.parameterize.underscore}", Spree::Product.find_by_name!(product_name))
    instance_variable_get("@#{product_name.parameterize.underscore}").option_types = [packaging]
    instance_variable_get("@#{product_name.parameterize.underscore}").save!
  end
end
