after :tax_categories, :shipping_categories do
  beverage = Spree::TaxCategory.find_by_name!("Beverage")
  shipping_category = Spree::ShippingCategory.find_by_name!("Default")

  default_attrs = {
    :available_on => Time.zone.now
  }

  products = [
    {
      :name => "Spring White",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "104.00",
      :description => "White tea from the Himalayas. Flowery, grassy, vegetal aromas with mild and sweet flavor.",
    },
    {
      :name => "Black Ruby",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "94.00",
      :description => "The culture of Ruby is quite old and local women still makes this kind of tea at their homes. Usually Ruby is handrolled followed by Mechanical rolling to give a fine twist, it is completely oxidized, dried slowly and matured with time and perfection. The more you keep your Ruby the better it becomes.",
    },
    {
      :name => "Himalayan Gold",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "94.00",
      :description => "Himalayan Gold is a very complex tea because it is long oxidized, naturally dried for several hours and then fired. The twisted golden tips are a beautiful contrast to the long, dark leaves that are almost black in color and amazingly fragrant. The leaves infuse to a rich auburn cup that boasts an incredible aroma with hints of vanilla, apricot and peach. The flavor is everything you would expect - smooth and naturally sweet with a fruity finish. Although, the dry leaves are impressively aggressive the cup or the liquor is mild as the Himalayan monsoon.",
    },
    {
      :name => "Himalayan Emerald",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "49.00",
      :description => "This green tea a rare variety! Soon after picking the fine and delicate buds of opening tea leaves during the period 'Second Flush' (mid April - May), they are handled with extreme care and steamed so that they are not led to be ‘enzymed’. Therefore, Himalayan Emerald Green Tea is not rolled nor oxidized and the pristine leaves are often left intact.",
    },
    {
      :name => "Himalayan Bouquet",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "64",
      :description => "Oolong tea partially oxidized.",
    },
    {
      :name => "Silver Tips",
      :tax_category => beverage,
      :shipping_category => shipping_category,
      :price => "119.00",
      :description => "Himalayan White Tea with smooth, fresh and sweet flavors similar to cooked peas and honey.",
    }
  ]

  products.each do |product_attrs|
    Spree::Config[:currency] = "DKK"

    default_shipping_category = Spree::ShippingCategory.find_by_name!("Default")
    product = Spree::Product.create!(default_attrs.merge(product_attrs))
    product.reload
    product.shipping_category = default_shipping_category
    product.save!
  end

  Spree::Config[:currency] = "DKK"
end
