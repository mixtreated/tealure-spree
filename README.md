# TEALURE SPREECOMMERCE FRONTEND

This README document contains resources that will help getting up to
speed with Tealure's development processes.

## Setting up your development environment

### Install Rails
Follow the guide at [installrails.com](http://www.installrails.com/)

### Environment-specific instructions
Depending on your development environment, you will find additional information in one of the following notes:
* [Nitrous.io](docs/nitrous.md)


### Productivity tips
To make sure you thrive at keeping a lean development environnement, here are the [productivity guidelines](docs/productivity.md)

## First-time app initialisation
Make sure your ruby version is `2.1.5`

Run the following commands in a sequence:

    bundle install
    bundle exec rake db:reset
    bundle exec rake spree_sample:load

## How to run the test suite
To be completed.

## Git Workflow
To be completed.

## Emails
We use [Mailcatcher]() in development to test emails. Install it locally with `gem install mailcatcher` to avoid conflicts.

## Deployment instructions
To be completed.

## Team collaboration

We use [slack](http://tealure.slack.io) for integrated instant messaging (only tech team
for now).

Our [trello board](https://trello.com/b/kBlnSAec/tealure-dev) is the place for getting an overview of the development activities and assigning yourself new tasks.

Our [G+ Community](https://plus.google.com/communities/117554038142889219098) is the place to share anything with the whole team, organize events etc...

We use [Nitrous](https://www.nitrous.io/) for [pair-programming](http://en.wikipedia.org/wiki/Pair_programming).
