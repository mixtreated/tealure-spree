angular.module('Spreetl').directive 'scrollable', ($window) ->
  (scope) ->
    angular.element($window).bind "scroll", ->
      if this.pageYOffset > 0
        scope.scrolled = true
      else
        scope.scrolled = false
      scope.$apply()
