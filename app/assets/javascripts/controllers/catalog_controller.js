'use strict';
angular.module('Spreetl').controller('CatalogController', ['$scope', function($scope) {
  /* Methods */
  $scope.activate = function(product){
    $scope.active = product;
    $scope.active.currentOption = $scope.active.options[0];
  };

  $scope.isActive= function() {
    return angular.isDefined($scope.active);
  };
}]);
