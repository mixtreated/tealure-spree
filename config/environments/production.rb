Rails.application.configure do
  config.paperclip_defaults = {
    :storage => :s3,
    :s3_host_name => 's3.eu-central-1.amazonaws.com',
    :s3_credentials => {
      :bucket => ENV['S3_BUCKET']
    }
  }

  config.assets.enabled = true
  config.assets.digest = true
  config.assets.initialize_on_precompile = true
  config.assets.js_compressor = :uglifier

  config.eager_load = true

  config.force_ssl = true

  # Email
  config.action_mailer.default_url_options = { host: 'tealure.com' }
  config.action_mailer.smtp_settings = {
    :port                 => ENV['MAILGUN_SMTP_PORT'],
    :address              => ENV['MAILGUN_SMTP_SERVER'],
    :user_name            => ENV['MAILGUN_SMTP_LOGIN'],
    :password             => ENV['MAILGUN_SMTP_PASSWORD'],
    :domain               => 'tealure.com',
    :port                 => 587,
    :authentication       => :plain,
    :enable_starttls_auto => true,
  }

  # Prerender.io
  config.middleware.use Rack::Prerender, prerender_token: ENV['PRERENDER_TOKEN']
end
