# Productivity guidelines

There is various way you can make you development environment more
productive.

Here are our recommendations.

## Use ZSH plugins
Zsh is a powerful shell language that will leverage the quality and
efficiency of working with the command line.

Before you can use it you need to choose Zsh instead of Bash as a
default shell.

    > chsh
    > Changing the login shell for action
    Enter the new value, or press ENTER for the default
      Login Shell [/bin/bash]: /bin/zsh

Note: you might want to migrate specific settings from `.bash_profile` or
`.bashrc` (e.g. environment variables in a Nitrous.io Box).

Then, explore these good resources to figure out what solution you'd
like to use:

* [Awesome ZSH Plugins](https://github.com/unixorn/awesome-zsh-plugins)
* [Antigen](http://antigen.sharats.me/)


## Finding the right text editor for you

## Setup your dev-env in the cloud
You can use services like Nitrous.io, NineCloud...
