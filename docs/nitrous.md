## Nitrous.io installalation notes

You can follow [Railsgirls](http://guides.railsgirls.com/install/#using-a-cloud-service) guide to setup your Nitrous.io development box.

### Allowing commits to BitBuckets
Once your account is setup, you need to allow it to access our BitBucket repository.
Simply go over to your user account page on BitBucket in the section
`Security > SSH keys`. Clic `Add key` then add the Nitrou.io key
displayed in your Box on [this page](https://www.nitrous.io/app#/boxes)
(you'll have to click on "Reveal public key").
Providing your Bitbucket user has access to our repository, you should
now be able to interact with it.

### Cloning the repository
In you Nitrous.io shell, run the following command in your
`~/workspace/` directory:

    git clone git@bitbucket.org:fuzzybird/tealure-spree.git

### Using the right Ruby version
You need to make sure you're using the ruby version specified in the
Gemfile, otherwise `bundle install` won't run. You can list the ruby
versions with Autoparts:

    parts search ruby

and then install the right version if it's showing up in the list:

    parts install ruby2.1

If it's not showing up, install ruby_install

    parts install ruby_install

And then use is to install the desired ruby version:

    ruby-install ruby 2.1.5

### Setting up Postgresql Heroku

Follow [This guide](http://blog.nitrous.io/2013/02/11/postgres-action-io-3.html).

Once setup, run

    rake db:create db:seed
    rake spree_sample:load

### SSH Client Access
This is optional and allows you to run terminal command in your own
shell instead of through the Web interface.

Generate a public/private keypair on your computer ([Github's guide until step 4](https://help.github.com/articles/generating-ssh-keys/)) then add the *public* key to [this page](https://www.nitrous.io/app#/public_keys).
You should now be able to establish an SSH connection with the address that is displayed when you click on your Box on [this page](https://www.nitrous.io/app#/boxes). Note that for an uri of the form `ssh://action@<id>.nitrousbox.com:<port>` the SSH command will be `ssh action@<id>.nitrousbox.com -p <port>`



